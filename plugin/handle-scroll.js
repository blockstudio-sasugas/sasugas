import Vue from "vue";

Vue.directive("scroll", {
  inserted: function (element, binding) {
    let customEvent = function (event) {
      if (binding.value(event, element)) {
        window.removeEventListener("scroll", customEvent);
      }
    };
    window.addEventListener("scroll", customEvent);
  },
});
